<?php
register_nav_menus( array(
"menu-principal" => "Menu principal"
) );
?>

<?php
get_header();
if (have_posts()) :
   while (have_posts()) :
      the_post();
      the_content();
   endwhile;
endif;

?>