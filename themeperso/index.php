<?php
get_header();?>
<section class="container-fluid">
<div class="kids-squad d-lg-block d-sm-none d-none">
    <section class="desktop-section">
    <ul class="hero image ri-true-fw">
      <div class="slide-one image">
          <a href="#" class="btn btn-lg active  burton rounded-pill" role="button" aria-pressed="true">Nouveautés</a>
      </div>
      <div class="slide-two image">
          <a href="#" class="btn btn-lg active  burton rounded-pill" role="button" aria-pressed="true">Hommes</a>
      </div>
      <div class="slide-three image">
          <a href="#" class="btn btn-lg active  burton rounded-pill" role="button" aria-pressed="true">Femmes</a>
      </div>
      <div class="slide-four image">
          <a href="#" class="btn  btn-lg active  burton rounded-pill" role="button" aria-pressed="true">Enfants</a>
      </div>
    </ul>
    </section>
  </div>
  <section class="container-fluid d-lg-none d-sm-block col-sm-12">
    <div class="row">
      <div class="card-img col-sm-6 taille" style="width: 25rem; height: 20rem;">
      <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/rando.jpg">
          <div class="card-img-overlay ">
              <a href="#" class="btn  btn-lg active rounded-pill burton2" role="button" aria-pressed="true">Nouveautés</a>
          </div>
        </div>
        <div class="card-img col-sm-6 taille" style="width: 25rem; height: 20rem;" >
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/homme.jpg">
            <div class="card-img-overlay">
                <a href="#" class="btn  btn-lg active  rounded-pill burton2" role="button" aria-pressed="true">Hommes</a>
            </div>
          </div>
          <div class="card-img col-sm-6 taille" style="width: 25rem; height: 20rem;">
          <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/femme2.png">
              <div class="card-img-overlay ">
                  <a href="#" class="btn  btn-lg active  rounded-pill burton2" role="button" aria-pressed="true">Femmes</a>
              </div>
            </div>
            <div class="card-img col-sm-6 taille" style="width: 25rem; height: 20rem;">
            <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/enfant.jpg">
                <div class="card-img-overlay "> 
                    <a href="#" class="btn  btn-lg active  rounded-pill burton2" role="button" aria-pressed="true">Enfants</a>
                </div>
            </div>
          </div>
</section>

</section>
<p class="text-uppercase font-weight-bold mt-5 text-center titre_new">nouveautés</p>
<section class="container-fluid mt-5 ">
<div class="row ">
    <div class="col-sm-3  article">
        <div class="card text-center touille" style="width: 18rem;">
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/nouveaute1.jpg">
            <div class="card-body">
              <p class="card-text">New Balance | Chaussures | Homme </p>
              <p class="card-text font-weight-bold">Quicka Rn</p>
              <p class="card-text font-weight-bold text-danger h4">64.99€</p>
              <a href="#" class="btn  lescartes">Voir l'article</a>
            </div>
          </div>
    </div>
    <div class="col-sm-3  article">
        <div class="card text-center touille" style="width: 18rem;">
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/nouveaute2.jpg">
            <div class="card-body">
                <p class="card-text">Under armour | Vêtement | Homme </p>
              <p class="card-text font-weight-bold">Tee-shirt Straker 2.0</p>
              <p class="card-text font-weight-bold text-danger h4">24.99€</p>
              <a href="#" class="btn lescartes">Voir l'article</a>
            </div>
          </div>
    </div>
    <div class="col-sm-3  article">
        <div class="card text-center touille" style="width: 18rem;">
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/nouveaute3.jpg">
            <div class="card-body">
                <p class="card-text">Asics | Vêtement | Femme </p>
                <p class="card-text font-weight-bold">Polo sport rouge</p>
                <p class="card-text font-weight-bold text-danger h4">14.99€</p>
                <a href="#" class="btn lescartes">Voir l'article</a>
            </div>
          </div>
    </div>
    <div class="col-sm-3  article">
        <div class="card text-center touille" style="width: 18rem;">
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/04/nouveaute4.jpg">
            <div class="card-body">
                <p class="card-text">Asics | Chaussures | Homme </p>
              <p class="card-text font-weight-bold">Gel-cumulus 20</p>
              <p class="card-text font-weight-bold text-danger h4">89.99€</p>
              <a href="#" class="btn lescartes">Voir l'article</a>
            </div>
          </div>
    </div>
  </div>
</section>
<section class="container-fluid mt-5" >
<div class="row justify-content-center">
<div class=" text-white col-12 merde card-img" style="height: 50vh">

    <div class="card-img-overlay">
      <h3 class="card-title text-uppercase font-weight-bold votre">votre magasin pres de chez vous</h3>
      <p class="card-text text-uppercase font-weight-bold horraires">horraires, informations, disponibilité</p>
      <p class="card-text horraires">Consultez les horraires, le plan d'accès et les informations de votre magasin en un clic.</p>
      <p class="card-text horraires">Consultez la disponibilité de vos articles.</p>
      <p class="card-text horraires">Commandez en ligne et retirez votre commande dans votre magasin</p>
      <button type="button" class="btn btn-outline-light btn-lg text-uppercase font-weight-bold">voir le magasin</button>
    </div>
  </div>
</div>
</section>
<?php get_footer(); 
?>